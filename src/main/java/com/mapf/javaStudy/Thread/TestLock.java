package com.mapf.javaStudy.Thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author mapf
 *ReentrantLock 与synchronized有相同的并发性和内存语义，还包含了中断锁等候和定时锁等候，意味着线程A如果先获得了对象obj的锁，那么线程B可以在等待指定时间内依然无法获取锁，那么就会自动放弃该锁。
 
但是由于synchronized是在JVM层面实现的，因此系统可以监控锁的释放与否，而ReentrantLock使用代码实现的，系统无法自动释放锁，需要在代码中finally子句中显式释放锁lock.unlock();
 */
public class TestLock {
	public static void main(String[] args) {
		 Lock lock = new ReentrantLock();
	     
	     ConsumerLock consumer = new ConsumerLock(lock);
	     ProducerLock producer = new ProducerLock(lock);
	     
	      new Thread(consumer).start();
	      new Thread( producer).start();
	}
	
}
