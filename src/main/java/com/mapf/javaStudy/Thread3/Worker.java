package com.mapf.javaStudy.Thread3;


public class Worker {

	
	public synchronized void executeA(String name) {
		for (int i = 0; i < 10; i++) {
			System.out.println(name + "-executeA-" + i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public synchronized void executeB(String name) {
		for (int i = 0; i < 10; i++) {
			System.out.println(name + "-executeB-" + i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized static void executeC(String name) {
		for (int i = 0; i < 10; i++) {
			System.out.println(name + "-executeC-" + i);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private class SynchronizerWorkerA extends Thread {
		
		public void run() {
			executeA("thread1");
		}
	}
	
	private class SynchronizerWorkerB extends Thread {
		public void run() {
			executeB("thread2");
		}
	}
	
	private class SynchronizerWorkerC extends Thread {
		public void run() {
			executeC("thread3");
		}
	}
	
	/**可以看到，A方法和C方法可以同时调用，而B则被锁着，直到A方法的锁释放后才能被调用。原因在于静态方法的锁锁定class，而一般方法的锁锁定对象(效果上可以这么认为)

	 * @param args
	 */
	public static void main(String args[]) {
		
		Worker worker = new Worker();
		
		worker.new SynchronizerWorkerA().start();
		worker.new SynchronizerWorkerB().start();
		worker.new SynchronizerWorkerC().start();
	}
}
