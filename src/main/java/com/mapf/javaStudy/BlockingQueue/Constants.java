package com.mapf.javaStudy.BlockingQueue;

public class Constants {
	  public static final int MAX_BUFFER_SIZE = 10;
	    public static final int NUM_OF_PRODUCER = 2;
	    public static final int NUM_OF_CONSUMER = 3;
}
