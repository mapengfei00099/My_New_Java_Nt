package com.mapf.javaStudy.Thread4;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 　　这个类是为了帮助猿友们方便的实现多个线程一起启动的场景，就像赛跑一样，只要大家都准备好了，那就开始一起冲。比如下面这个程序，所有的线程都准备好了，
 * 才会一起开始执行。
 * 
 * 
 * @author zuoxiaolong
 *
 */
public class CyclicBarrierTest {

	public static void main(String[] args) {
		final CyclicBarrier cyclicBarrier = new CyclicBarrier(10);
		for (int i = 0; i < 10; i++) {
			final int number = i + 1;
			Runnable runnable = new Runnable() {
				public void run() {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
					System.out.println("等待执行任务[" + number + "]");
					try {
						cyclicBarrier.await();
					} catch (InterruptedException e) {
					} catch (BrokenBarrierException e) {
					}
					System.out.println("开始执行任务[" + number + "]");
				}
			};
			Thread thread = new Thread(runnable);
			thread.start();
		}
	}

}
